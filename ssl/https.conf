upstream termination {
	server 127.0.0.1:80;
}

server {
    # Enable HTTP/2 (optional).
    listen                          443 ssl http2;
	
    server_name                     localhost; # Change localhost to your domain

    include                         /etc/nginx/conf.d/ips-cloudflare.conf;
	include                         /etc/nginx/conf.d/timeout.conf;
    include                         /etc/nginx/conf.d/ssl.conf;

    # Cert
	ssl_certificate                 /etc/nginx/ssl/cert/cert.pem;
	ssl_certificate_key             /etc/nginx/ssl/cert/key.pem;

	location / {
		proxy_pass                  http://termination;
		proxy_set_header            Host $http_host;
        proxy_set_header            X-Forwarded-Host $http_host;
        proxy_set_header            X-Real-IP $remote_addr;
        proxy_set_header            X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header            X-Forwarded-Proto 'https';
        proxy_set_header            X-Forwarded-Port '443';
        proxy_redirect              off;
		proxy_set_header            X-Node $hostname;

        # Timeout
        proxy_send_timeout          120s;
        proxy_read_timeout          120s;
        send_timeout                120s;

        #http2 & http3
        add_header                  'X-Forwarded-Proto' 'http/2';
        add_header                  'X-Forwarded-Proto' 'h2';
        add_header                  'X-Forwarded-Proto' 'h3';

        modsecurity on; # Comment this 2 lines for disabling owasp mosecurity
        modsecurity_rules_file      /etc/nginx/modsecurity/main.conf;
        include                     /etc/nginx/conf.d/compress.conf;
	}

    include                         /etc/nginx/conf.d/pagespeed.conf; # Pagespeed
    include                         /etc/nginx/conf.d/location.conf;
    include                         /etc/nginx/conf.d/error.conf; # Error Page

    access_log                      /var/log/nginx/ssl_web_access.log custom;
	error_log                       /var/log/nginx/ssl_web_error.log error;

}