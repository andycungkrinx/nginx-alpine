ARG BASE_IMAGE_TAG=3.8

FROM alpine:${BASE_IMAGE_TAG}

LABEL maintainer="Andy Cungkrinx <andy.silva270114@gmail.com>" \
    version.alpine="3.8" \
    version.nginx="1.20.0" \
    version.mod-pagespeed="v1.13.35.2" \
    version.ngx-pagespeed="v1.13.35.2" \
    version.ngx-modsecurity="1.0.0" \
    version.modsecurity="3.0.4" \
    version.owasp="3.2.0"

ARG NGINX_VER=1.20.0

ENV NGINX_VER="${NGINX_VER}" \
    APP_ROOT="/var/www/html"

RUN apk --no-cache add shadow; \
    set -ex; \
    nginx_up_ver="0.9.1"; \
    ngx_pagespeed_ver="1.13.35.2"; \
    mod_pagespeed_ver="1.13.35.2"; \
    ngx_modsecurity_ver="1.0.0"; \
    modsecurity_ver="3.0.4"; \
    owasp_crs_ver="3.2.0"; \
    \
    addgroup -S app; \
    adduser -S -D -H -h /var/cache/nginx -s /sbin/nologin -G app app; \
    usermod -u 1000 app; \
    groupmod -g 1000 app; \
    \
    apk add --update --no-cache -t .tools \
        ca-certificates \
        curl \
        gzip \
        tar \
        unzip \
        findutils \
        make \
        nghttp2 \
        yajl \
        geoip; \
    \
    apk add --update --no-cache -t .nginx-build-deps \
        ca-certificates \
        openssl \
        curl \
        gzip \
        tar \
        unzip \
        wget \
        gnupg \
        findutils \
        make \
        nghttp2 \
        linux-pam-dev \
        apr-dev \
        apr-util-dev \
        build-base \
        gd-dev \
        git \
        util-linux-dev \
        gnupg \
        gperf \
        perl \
        icu-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libressl-dev \
        libtool \
        libxslt-dev \
        linux-headers \
        pcre-dev \
        geoip-dev \
        libmaxminddb-dev \
        zlib-dev \
        autoconf \
        automake \
        bison \
        curl \
        g++ \
        cmake \
        git \
        libuuid \
        libxml2-dev \
        go \
        patch; \
    \
    apk add --no-cache -t .libmodsecurity-build-deps \
        autoconf \
        automake \
        bison \
        curl \
        flex \
        g++ \
        git \
        libmaxminddb-dev \
        libstdc++ \
        libtool \
        libxml2-dev \
        pcre-dev \
        rsync \
        sed \
        yajl \
        yajl-dev; \
    \
    # @todo download from main repo when updated to alpine 3.10.
    apk add -U --no-cache -t .nginx-edge-build-deps -X http://dl-cdn.alpinelinux.org/alpine/edge/main/ brotli-dev; \
    # Modsecurity lib.
    cd /tmp; \
    git clone --depth 1 -b "v${modsecurity_ver}" --single-branch https://github.com/SpiderLabs/ModSecurity; \
    cd ModSecurity; \
    git submodule init;  \
    git submodule update; \
    ./build.sh; \
    ./configure --disable-doxygen-doc --disable-doxygen-html; \
    make -j2; \
    make install;  \
    mkdir -p /etc/nginx/modsecurity/; \
    mv modsecurity.conf-recommended /etc/nginx/modsecurity/modsecurity.conf;  \
    sed -i 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' /etc/nginx/modsecurity/modsecurity.conf; \
    cp unicode.mapping /etc/nginx/modsecurity/; \
    rsync -a --links /usr/local/modsecurity/lib/libmodsecurity.so* /usr/local/lib/; \
    \
    # Brotli.
    cd /tmp; \
    git clone https://github.com/google/ngx_brotli.git; \
    cd /tmp/ngx_brotli; \
    git submodule update --init ; \
    \
    # Get ngx modsecurity module.
    cd /tmp; \
    mkdir -p /tmp/ngx_http_modsecurity_module; \
    ver="${ngx_modsecurity_ver}"; \
    url="https://github.com/SpiderLabs/ModSecurity-nginx/releases/download/v${ver}/modsecurity-nginx-v${ver}.tar.gz"; \
    wget -qO- "${url}" | tar xz --strip-components=1 -C /tmp/ngx_http_modsecurity_module; \
    \
    # OWASP.
    cd /tmp; \
    wget -qO- "https://github.com/SpiderLabs/owasp-modsecurity-crs/archive/v${owasp_crs_ver}.tar.gz" | tar xz -C /tmp; \
    cd /tmp/owasp-modsecurity-crs-*; \
    sed -i "s#SecRule REQUEST_COOKIES|#SecRule REQUEST_URI|REQUEST_COOKIES|#" rules/REQUEST-941-APPLICATION-ATTACK-XSS.conf; \
    mkdir -p /etc/nginx/modsecurity/crs/; \
    mv crs-setup.conf.example /etc/nginx/modsecurity/crs/setup.conf; \
    mv rules /etc/nginx/modsecurity/crs; \
    \
    # Get ngx pagespeed module.
    cd /tmp; \
    git clone -b "v${ngx_pagespeed_ver}-stable" \
          --recurse-submodules \
          --shallow-submodules \
          --depth=100 \
          -c advice.detachedHead=false \
          -j2 \
          https://github.com/apache/incubator-pagespeed-ngx.git \
          /tmp/ngx_pagespeed; \
    \
    # Get psol for alpine.
    cd /tmp; \
    url="https://github.com/wodby/nginx-alpine-psol/releases/download/${mod_pagespeed_ver}/psol.tar.gz"; \
    wget -qO- "${url}" | tar xz -C /tmp/ngx_pagespeed; \
    \
    # Get ngx uploadprogress module.
    mkdir -p /tmp/ngx_http_uploadprogress_module; \
    url="https://github.com/masterzen/nginx-upload-progress-module/archive/v${nginx_up_ver}.tar.gz"; \
    wget -qO- "${url}" | tar xz --strip-components=1 -C /tmp/ngx_http_uploadprogress_module; \
    \
    # GeoIP module
    cd /tmp; \
    git clone https://github.com/leev/ngx_http_geoip2_module.git; \
    \
    # Auth PAM module
    cd /tmp; \
    wget https://github.com/sto/ngx_http_auth_pam_module/archive/v1.5.2.tar.gz; \
    tar -xvf v1.5.2.tar.gz; \
    mv ngx_http_auth_pam_module-1.5.2 ngx_http_auth_pam_module; \
    \
    # DAV ext module
    cd /tmp; \
    git clone https://github.com/arut/nginx-dav-ext-module.git; \
    \
    # Echo nginx module
    cd /tmp; \
    git clone https://github.com/openresty/echo-nginx-module.git; \
    \
    # Download nginx.
    cd /tmp; \
    curl -fSL "https://nginx.org/download/nginx-${NGINX_VER}.tar.gz" -o /tmp/nginx.tar.gz; \
    tar zxf /tmp/nginx.tar.gz -C /tmp; \
    \
    cd "/tmp/nginx-${NGINX_VER}"; \
    ./configure \
        --prefix=/etc/nginx \
        --sbin-path=/usr/sbin/nginx \
        --modules-path=/usr/lib/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --lock-path=/var/run/nginx.lock \
        --http-client-body-temp-path=/var/cache/nginx/client_temp \
        --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
        --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
        --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
        --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
        --build='By AndyCungkrinx | Pagespeed | Modsecurity OWASP | Brotli | GeoIP2 | Uploadprogress ' \
        --user=app \
        --group=app \
        --with-compat \
        --with-file-aio \
        --with-pcre-jit \
        --with-http_ssl_module \
        --with-http_stub_status_module \
        --with-http_realip_module \
        --with-http_auth_request_module \
        --with-http_v2_module \
        #--with-http_v3_module \
        --with-http_dav_module \
        --with-http_slice_module \
        --with-threads \
        --with-http_addition_module \
        --with-http_flv_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_secure_link_module \
        --with-http_sub_module \
        --with-mail_ssl_module \
        --with-stream_ssl_module \
        --with-stream_ssl_preread_module \
        --with-ipv6 \
        --with-ld-opt="-Wl,-z,relro,--start-group -lapr-1 -laprutil-1 -licudata -licuuc -lpng -lturbojpeg -ljpeg" \
        --with-stream_realip_module \
        --with-mail=dynamic \
        --with-stream=dynamic \
        --with-http_xslt_module=dynamic \
        --with-http_image_filter_module=dynamic \
        --with-http_geoip_module=dynamic \
        --add-dynamic-module=/tmp/ngx_http_uploadprogress_module \
        --add-dynamic-module=/tmp/ngx_brotli \
        --add-dynamic-module=/tmp/ngx_http_auth_pam_module \
        --add-dynamic-module=/tmp/nginx-dav-ext-module \
        --add-dynamic-module=/tmp/echo-nginx-module \
        --add-dynamic-module=/tmp/ngx_http_geoip2_module \
        --add-dynamic-module=/tmp/ngx_pagespeed \
        --add-dynamic-module=/tmp/ngx_http_modsecurity_module; \
    \
    make -j2; \
    make install; \
    \
    install -g app -o app -d \
        /var/cache/nginx \
        /var/cache/ngx_pagespeed \
        /pagespeed_static \
        /ngx_pagespeed_beacon; \
    \
    install -m 400 -d /etc/nginx/pki; \
    strip /usr/sbin/nginx*; \
    ln -s /usr/lib/nginx/modules /etc/nginx/modules; \
    strip /usr/lib/nginx/modules/*.so; \
    strip /usr/local/lib/libmodsecurity.so*; \
    \
    runDeps="$( \
        scanelf --needed --nobanner --format '%n#p' /usr/sbin/nginx /usr/local/modsecurity/lib/*.so /usr/lib/nginx/modules/*.so /tmp/envsubst \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --no-cache --virtual .nginx-rundeps $runDeps; \
    \
    apk del --purge .nginx-build-deps .nginx-edge-build-deps .libmodsecurity-build-deps; \
    ln -sf /dev/stdout /var/log/nginx/access.log; \
    ln -sf /dev/stderr /var/log/nginx/error.log; \
    rm -rf \
        /tmp/* \
        /usr/local/modsecurity \
        /var/cache/apk/* \
        /root/.cargo \
        /root/.rustup;

COPY conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY conf/nginx/conf.d /etc/nginx/conf.d
COPY ssl /etc/nginx/ssl
COPY conf/nginx/sites-enabled /etc/nginx/sites-enabled
COPY conf/nginx/modules/modules.conf /etc/nginx/modules/modules.conf
COPY conf/nginx/modsecurity/main.conf /etc/nginx/modsecurity/main.conf
COPY html /var/www/html

WORKDIR $APP_ROOT
EXPOSE 80 443

STOPSIGNAL SIGTERM

RUN apk del \
    wget \
    tar; \
    rm -rf /var/cache/apk/*; \
    rm -rf /tmp/*; \
    chown app:app /var/www/ -R; \
    cat /dev/null > /root/.ash_history && exit;

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]