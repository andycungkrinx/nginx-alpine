# Alpine Nginx
```sh
Dockerize nginx, pagespeed and modsecurity base on alpine
Default master using Nginx 1.20.0
```

## Feature
```sh
  * Nginx 1.20.0
  * PageSpeed Nginx module 1.13.35.2
  * PageSpeed Library 1.13.35.2
  * ModSecurity Nginx module 1.0.0
  * Brotli
  * ModSecurity Library 3.0.4
  * OWASP CRS 3.2.0
```
